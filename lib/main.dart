import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fire_ss/screens/homescreen.dart';
import 'package:flutter_fire_ss/screens/signinscreen.dart';
import 'package:flutter_fire_ss/screens/signupscreen.dart';
import 'package:flutter_fire_ss/screens/welcomescreen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: WelcomeScreen(),
      routes: <String, WidgetBuilder>{
        '/landingscreen': (BuildContext context) => WelcomeScreen(),
        '/signupscreen': (BuildContext context) => SignUpScreen(),
        '/signinscreen': (BuildContext context) => SignInScreen(),
        '/homescreen': (BuildContext context) => HomeScreen()
      },
    );
  }
}

import 'package:flutter/material.dart';

import '../services/customfirebaseservice.dart';

class SignInScreen extends StatefulWidget {
  @override
  _SignInScreenState createState() => _SignInScreenState();
}

class _SignInScreenState extends State<SignInScreen> {
  String phoneNumber, snackBarText;
  final GlobalKey<ScaffoldState> signInScaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: signInScaffoldKey,
      body: Center(
        child: Container(
          padding: EdgeInsets.all(25.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextField(
                decoration: InputDecoration(hintText: "Phone Number"),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    phoneNumber = value;
                  });
                },
              ),
              SizedBox(height: 15),
              RaisedButton(
                child: Text("Login with Phone Number"),
                color: Colors.tealAccent,
                textColor: Colors.black,
                elevation: 7.0,
                onPressed: () {
                  print("Login Pressed");
                  FocusScope.of(context).requestFocus(FocusNode());
                  validatePhoneNumber();
                  signInScaffoldKey.currentState
                      .showSnackBar(SnackBar(content: Text(snackBarText)));
                },
              ),
              SizedBox(height: 15),
              Text("Don't have an account?"),
              SizedBox(height: 10),
              RaisedButton(
                child: Text("SignUp"),
                color: Colors.orange,
                textColor: Colors.white,
                elevation: 7.0,
                onPressed: () {
                  Navigator.of(context).pushReplacementNamed('/signupscreen');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Validating phone number
  validatePhoneNumber() {
    if (phoneNumber == null || phoneNumber.length != 10) {
      snackBarText = "Phone number must be 10 digits without country code";
    } else {
      snackBarText = "Please wait..!!";
      // Firebase service call to check user
      CustomFirebaseService()
          .checkUserExistInFireStore(phoneNumber, context, signInScaffoldKey);
    }
  }
}

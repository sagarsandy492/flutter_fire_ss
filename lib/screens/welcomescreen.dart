import 'package:flutter/material.dart';
import 'package:flutter_fire_ss/widgets/button_widget.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            GestureDetector(
              onTap: () => Navigator.of(context).pushNamed('/signinscreen'),
              child: ButtonWidget("SignIn", [
                Color(0xFFFF5F6D),
                Color(0xFFFFC371),
              ]),
            ),
            SizedBox(height: 30),
            GestureDetector(
              onTap: () => Navigator.of(context).pushNamed('/signupscreen'),
              child: ButtonWidget("SignUp", [
                Color(0xFFA1FFCE),
                Color(0xFFFF5F6D),
              ]),
            )
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_fire_ss/services/customfirebaseservice.dart';

class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  String phoneNumber, snackBarText;
  final GlobalKey<ScaffoldState> signUpScaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: signUpScaffoldKey,
      body: Center(
        child: Container(
          padding: EdgeInsets.all(25.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              TextField(
                decoration: InputDecoration(hintText: "Phone Number"),
                keyboardType: TextInputType.number,
                onChanged: (value) {
                  setState(() {
                    phoneNumber = value;
                  });
                },
              ),
              SizedBox(height: 15),
              RaisedButton(
                child: Text(
                  "Sign Up",
                  style: TextStyle(color: Colors.black),
                ),
                color: Colors.tealAccent,
                textColor: Colors.white,
                elevation: 7.0,
                onPressed: () {
                  print('Sign up clicked');
                  FocusScope.of(context).requestFocus(FocusNode());
                  validatePhoneNumber();
                  signUpScaffoldKey.currentState
                      .showSnackBar(SnackBar(content: Text(snackBarText)));
                },
              ),
              SizedBox(height: 15),
              Text("Already have an account?"),
              SizedBox(height: 10),
              RaisedButton(
                child: Text("SignIn"),
                color: Colors.orange,
                textColor: Colors.white,
                elevation: 7.0,
                onPressed: () {
                  Navigator.of(context).pushReplacementNamed('/signinscreen');
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Validating phone number
  validatePhoneNumber() {
    if (phoneNumber == null || phoneNumber.length != 10) {
      snackBarText = "Phone number must be 10 digits without country code";
    } else {
      snackBarText = "Please wait..!!";
      // Firebase service call to save user
      CustomFirebaseService()
          .registerWithPhoneNumber(phoneNumber, context, signUpScaffoldKey);
    }
  }
}

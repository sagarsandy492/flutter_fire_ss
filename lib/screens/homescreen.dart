import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_fire_ss/screens/paypalpayment.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  String ordernumber = "";
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Welcome..."),
        centerTitle: true,
      ),
      body: Center(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Visibility(
                          visible: ordernumber == "" ? false : true,
                          child: Text(
                            'Payment Success\n\norder id: ' + ordernumber,
                            textAlign: TextAlign.center,
                          )),
                      RaisedButton(
                        onPressed: () {
                          // make PayPal payment

                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (BuildContext context) => PaypalPayment(
                                onFinish: (number) async {
                                  // payment done
                                  setState(() {
                                    ordernumber = number;
                                  });
                                  print('order id: ' + number);
                                },
                              ),
                            ),
                          );
                        },
                        child: Text(
                          'Pay with Paypal',
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 15),
              OutlineButton(
                onPressed: () {
                  FirebaseAuth.instance.signOut().then((value) {
                    Navigator.of(context)
                        .pushReplacementNamed('/landingscreen');
                  }).catchError((e) {
                    print(e);
                  });
                },
                borderSide: BorderSide(
                    color: Colors.red, style: BorderStyle.solid, width: 4.0),
                child: Text("Logout"),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';

class UserService {
  storeNewUser(uid, phone, context) {
    print('user service called');
    FirebaseFirestore.instance
        .collection('/users')
        .add({'phone': phone, 'uid': uid}).then((value) {
      Navigator.of(context).popAndPushNamed("/homescreen");
    }).catchError((error) {
      print(error);
    });
  }
}

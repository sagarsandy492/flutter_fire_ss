import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_fire_ss/services/userservice.dart';

class CustomFirebaseService {
  // Register firebase user with phone number
  registerWithPhoneNumber(
      phoneNumber, context, GlobalKey<ScaffoldState> scaffoldKey) async {
    print(phoneNumber); // User entered phone number
    String phoneNumberWithCountryCode =
        '+91 ' + phoneNumber; // Appending user phone number with country code
    print(phoneNumberWithCountryCode);
    await FirebaseAuth.instance.verifyPhoneNumber(
      phoneNumber: phoneNumberWithCountryCode,
      timeout: Duration(seconds: 60),
      verificationCompleted: (PhoneAuthCredential credential) {
        print("This is auto OTP verification block");
        // sign in user with credentials
        signInUserWithPhoneNumber(
            credential, phoneNumber, context, scaffoldKey);
      },
      verificationFailed: (FirebaseAuthException e) {
        print("Phone number auth verification failed");
        scaffoldKey.currentState.showSnackBar(SnackBar(
            content: Text("Verification failed, please check phone number")));
        print(e);
      },
      codeSent: (String verificationId, int resendToken) {
        // Emulator testing: Create a PhoneAuthCredential with the OTP code
        String smsCode = '123456'; // Test OTP need to add in firebase app
        PhoneAuthCredential phoneAuthCredential = PhoneAuthProvider.credential(
            verificationId: verificationId, smsCode: smsCode);
        // sign in user with credentials
        signInUserWithPhoneNumber(
            phoneAuthCredential, phoneNumber, context, scaffoldKey);
      },
      codeAutoRetrievalTimeout: (String verificationId) {
        scaffoldKey.currentState.showSnackBar(
            SnackBar(content: Text("OTP timeout, sending new one.")));
        print('First OTP time out, sending new one');
      },
    );
  }

  // Sign in user with phone auth credentials
  signInUserWithPhoneNumber(
      phoneAuthCredential, phoneNumber, context, scaffoldKey) {
    // Sign the user in (or link) with the credential
    try {
      FirebaseAuth.instance
          .signInWithCredential(phoneAuthCredential)
          .then((value) {
        print("Sign Success");
        print(FirebaseAuth.instance.currentUser.uid);
        print(FirebaseAuth.instance.currentUser.phoneNumber);
        UserService().storeNewUser(
            FirebaseAuth.instance.currentUser.uid, phoneNumber, context);
      });
    } catch (e) {
      print("Sign in with phone auth went wrong");
      scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text("Please try again!!")));
      print(e.toString());
    }
  }

  //Checking user phone number in fire store database
  checkUserExistInFireStore(
      phoneNumber, context, GlobalKey<ScaffoldState> scaffoldKey) {
    try {
      FirebaseFirestore.instance
          .collection("/users")
          .where('phone', isEqualTo: phoneNumber)
          .limit(1)
          .get()
          .then((QuerySnapshot querySnapshot) {
        if (querySnapshot.docs.isNotEmpty) {
          print("User found!!");
          Navigator.of(context).popAndPushNamed("/homescreen");
        } else {
          scaffoldKey.currentState.showSnackBar(
              SnackBar(content: Text("New user?, please click on Sign Up")));
          print("User not found");
        }
      });
    } catch (e) {
      print("Something went wrong with checking user in firestore");
      scaffoldKey.currentState
          .showSnackBar(SnackBar(content: Text("Please try again!!")));
      print(e.toString());
    }
  }
}
